package br.com.empresa.repository;

import br.com.empresa.models.Empresa;
import org.springframework.data.repository.CrudRepository;

public interface EmpresaRepository extends CrudRepository<Empresa, String> {
}
