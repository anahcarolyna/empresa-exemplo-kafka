package br.com.empresa.controller;

import br.com.empresa.models.Empresa;
import br.com.empresa.service.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import br.com.empresa.producer.EmpresaProducer;

import javax.validation.Valid;

@RestController
public class EmpresaController {

    @Autowired
    private EmpresaService empresaService;

    @Autowired
    private EmpresaProducer producer;

    @PostMapping("/empresa")
    @ResponseStatus(HttpStatus.CREATED)
    public Empresa create(@RequestBody @Valid Empresa empresa) {

        producer.enviarAoKafka(empresa);
        return empresaService.create(empresa);
    }
}
