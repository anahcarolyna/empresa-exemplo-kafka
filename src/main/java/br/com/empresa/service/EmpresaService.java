package br.com.empresa.service;

import br.com.empresa.models.Empresa;
import br.com.empresa.repository.EmpresaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmpresaService {

    @Autowired
    private EmpresaRepository empresaRepository;

    public Empresa create(Empresa empresa){
        return empresaRepository.save(empresa);
    }
}
